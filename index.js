const express = require('express')
const async = require('async')
const request = require('request')
const bodyParser = require('body-parser')
const download = require('download')
const util = require('util')
const fs = require('fs')
const mime = require('mime-types')
const mkdirp = require('mkdirp')
const app = express()

const chanelToken =
	'34d54zSu8msCDGup4eettsvD3ND+bpcmzr64d7P7HWtE/ST31G9av0rD5ZUpFWGG/7QpLlDx7HqwAIOH3ffX+B8lBGU6pTwwq5zhGKzkf+r+4uN39eZ/6G4cNOUQR2rEnkM1X+LssvzrWM2Dh5osbQdB04t89/1O/w1cDnyilFU='

const PORT = 3099
const CACHE_INTERVAL = 300 // Seconds
const BASE_DIR = `${__dirname}//data`
const MESSAGE_CONFIG = `${__dirname}//messages.json`

let cacheStore = []
let messageStore = {}

const loadConfig = (cb = err => {}) => {
	fs.readFile(MESSAGE_CONFIG, (err, res) => {
		if (err) return cb(err)
		try {
			messageStore = JSON.parse(res)
		} catch (e) {
			return cb(err)
		}
		cb()
	})
}

const saveConfig = (cb = err => {}) => {
	fs.writeFile(MESSAGE_CONFIG, JSON.stringify(messageStore), cb)
}

const pad = num => {
	var s = '000000' + num
	return s.substr(s.length - 2)
}

const reply = (replyToken, messages, cb) => {
	if (cb === undefined) cb = (err, res) => {}
	request.post(
		'https://api.line.me/v2/bot/message/reply',
		{
			headers: {
				authorization: `Bearer ${chanelToken}`
			},
			body: {
				replyToken,
				messages
			},
			json: true
		},
		(e, r, b) => {
			if (e || !b) return cb('ERR_SENDING_REQ')
			if (!!b.message) return cb(b.message)
			else return cb(null, true)
		}
	)
}

const leaveGroup = (groupId, cb) => {
	request.post(
		`https://api.line.me/v2/bot/group/${groupId}/leave`,
		{
			headers: {
				authorization: `Bearer ${chanelToken}`
			},
			json: true
		},
		(e, r, b) => {
			if (e || !b) return cb('ERR_SENDING_REQ')
			if (!!b.message) return cb(b.message)
			else return cb(null, true)
		}
	)
}

const getName = (groupId, userId, cb) => {
	if (cb === undefined) cb = (err, res) => {}
	let cachedData = null
	cacheStore.forEach((cache, i) => {
		if (cache.groupId == groupId && cache.userId == userId) cachedData = cache.data
	})
	if (!!cachedData) {
		console.log('Fetch name from cache store!')
		return cb(null, cachedData)
	}
	request.get(
		`https://api.line.me/v2/bot/group/${groupId}/member/${userId}`,
		{
			headers: {
				authorization: `Bearer ${chanelToken}`
			},
			json: true
		},
		(e, r, b) => {
			if (e || !b) return cb('ERR_SENDING_REQ')
			const inserted = cacheStore.push({
				groupId,
				userId,
				data: b
			})
			setTimeout(() => {
				cacheStore.splice(inserted - 1, 1)
			}, CACHE_INTERVAL * 1000)
			console.log(`Name cached! Index: ${inserted}`)
			cb(null, b)
		}
	)
}

const getDisplayDate = d => {
	return `${pad(d.getDate())}-${pad(d.getMonth())}-${pad(d.getFullYear())}`
}

const getDisplayTime = d => {
	return `${pad(d.getHours())}:${pad(d.getMinutes())}:${pad(d.getSeconds())}`
}

const saveMessage = (groupId, data, cb) => {
	// Save to groupId_dd-mm-YY.txt
	// data = { name, text, timestamp }
	if (cb === undefined) cb = err => {}
	console.log('saveMessage', data)

	const { name, text, timestamp } = data
	const d = new Date(timestamp)
	const dirname = BASE_DIR + `//${groupId}`
	const filename = `${getDisplayDate(d)}.txt`
	const displayTime = `[${getDisplayTime(d)}]`

	if (text == '') return cb()

	mkdirp(dirname, err => {
		if (err) return cb('ERR_CREATING_DIR')
		fs.appendFile(`${dirname}//${filename}`, `${name} ${displayTime}: ${text}\n`, cb)
	})
}

const saveAttachment = (groupId, attachmentId, cb) => {
	// Pipe url to download queue
	if (cb === undefined) cb = err => {}
	console.log('saveAttachment', attachmentId)

	request.head(
		`https://api.line.me/v2/bot/message/${attachmentId}/content`,
		{
			headers: {
				authorization: `Bearer ${chanelToken}`
			}
		},
		(e, r, b) => {
			if (e || !r.statusCode == 200) return cb('ERR_SENDING_REQ')

			const extension = mime.extension(r.headers['content-type'])
			const filename = `${attachmentId}.${extension}`
			const dirname = BASE_DIR + `//${groupId}//attachments`

			mkdirp(dirname, err => {
				if (err) return cb('ERR_CREATING_DIR')
				request
					.get(`https://api.line.me/v2/bot/message/${attachmentId}/content`, {
						headers: {
							authorization: `Bearer ${chanelToken}`
						}
					})
					.pipe(fs.createWriteStream(`${dirname}//${filename}`))
				cb()
			})
		}
	)
}

const filterJoinGroup = event => {
	const { replyToken } = event
	// reply(replyToken, [{ type: 'text', text: 'สวัสดี! บอทกำลังเริ่มต้นบันทึกข้อความ... (พิมพ์ !bye เพื่อเตะออกจากกลุ่ม)' }])
}

const filterLeaveGroup = () => {}

const filterFollow = () => {}

const filterUnfollow = () => {}

const filterMessage = event => {
	const { timestamp, source, replyToken, message } = event
	const { groupId, userId } = source
	let name = userId

	if (source.type != 'group') return // For group only

	async.series([
		cb => {
			getName(groupId, userId, (err, res) => {
				if (err) {
					console.log("Error while getting user's name!", err)
					return cb()
				}
				const { displayName, userId, pictureUrl } = res
				name = displayName
				cb()
			})
		},
		cb => {
			let text = ''
			switch (message.type) {
				case 'text':
					text = message.text
					if (text == '!bye') {
						// reply(replyToken, [{ type: 'text', text: 'บ๊่ายบายย!!~' }], (err, res) => {
						leaveGroup(groupId, (err, res) => console.log(`Leave group ${groupId}`))
						// })
						// reply(replyToken, [{ type: 'text', text: 'กูไม่ออก!!!' }])
					} else if (text == '!status') {
						reply(replyToken, [{ type: 'text', text: `Group: ${groupId}\nTotal message learned: ${Object.keys(messageStore).length}` }])
					} else if (text.startsWith('!learn') && text.split(' ').length == 3) {
						const ask = text.split(' ')[1]
						const ans = text.split(' ')[2]
						messageStore[ask] = ans
						reply(replyToken, [{ type: 'text', text: `OK, Total ${Object.keys(messageStore).length} learned!` }])
						saveConfig()
					} else if (text == '!learned') {
						let m = `Total ${Object.keys(messageStore).length} learned!\n`
						let a = []
						Object.keys(messageStore).map((key, i) => {
							a.push(`#${i} ${key} = ${messageStore[key]}`)
						})
						m += a.join('\n')
						reply(replyToken, [{ type: 'text', text: m }])
					} else if (text.startsWith('!unlearn') && text.split(' ').length == 2) {
						let id = +text.split(' ')[1]
						let key = Object.keys(messageStore)[id]
						if (key) {
							delete messageStore[key]
						}
						reply(replyToken, [{ type: 'text', text: `OK, Total ${Object.keys(messageStore).length} learned!` }])
						saveConfig()
					} else {
						if (messageStore[text]) {
							reply(replyToken, [{ type: 'text', text: messageStore[text] }])
						}
					}
					break
				case 'image':
					text = `Sending image... ${message.id}`
					saveAttachment(groupId, message.id)
					break
				case 'video':
					text = `Sending video... ${message.id}`
					saveAttachment(groupId, message.id)
					break
				case 'audio':
					text = `Sending audio... ${message.id}`
					saveAttachment(groupId, message.id)
					break
				case 'file':
					text = `Sending file... ${message.id}`
					saveAttachment(groupId, message.id)
					break
				case 'sticker':
					text = `Sending sticker...`
					break
				case 'location':
					break
				default:
					break
			}
			saveMessage(
				groupId,
				{
					name,
					text,
					timestamp
				},
				err => {
					if (err) {
						console.log('Error while saving message!', err)
					} else {
						console.log('Saved message!')
					}
					cb()
				}
			)
		}
	])
}

app.use(bodyParser.json())
app.listen(PORT)
console.log(`Server is listening on port ${PORT}`)

loadConfig(err => {
	console.log(`${Object.keys(messageStore).length} messages loaded.`)
})

app.post('/', (req, res) => {
	const { body } = req
	if (!!body.events) {
		body.events.forEach(event => {
			switch (event.type) {
				case 'join':
					filterJoinGroup(event)
					break
				case 'leave':
					filterLeaveGroup(event)
					break
				case 'follow':
					filterFollow(event)
					break
				case 'unfollow':
					filterUnfollow(event)
					break
				case 'message':
					filterMessage(event)
					break
				default:
					break
			}
		})
	}
	// console.log(util.inspect(req.body, false, null))
	res.send('OK')
})
